$(function(){


	/*****************************************
		CARROSSEIS
	*******************************************/
		//CAROUSEL PHOTOS MAJU
		$("#carouselPhotosMaju").owlCarousel({
			items : 5,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,	       
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    smartSpeed: 450,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	        	1:{
	                items:1
	            },
	            320:{
	                items:1
	            },
	            445:{
	                items:2
	            },
	            600:{
	                items:3
	            },
	            767:{
	                items:3
	            },
	           
	            991:{
	                items:4
	            },
	            1024:{
	                items:5
	            },
	            1440:{
	                items:5
	            },
	            			            
	        }		    		   		    
		    
		});
		
		$('header .navbar button').click(function() {
	 		let verif = $(this).attr('aria-expanded');
	 		if(verif == "true" || verif == "undefined"){
	 			$(this).removeClass('exchangeButton');
	 		}else{
	 			$(this).addClass('exchangeButton');
	 		}
	 		
	 		setTimeout(function(){
	 			if(verif == "true" || verif == "undefined"){
	 				$(this).removeClass('exchangeButton');
	 			}else{
	 				$(this).addClass('exchangeButton');
	 			}
	 		}, 100);
	 	});
});